// EXPRESS Setup
const express = require('express');
const mongoose = require('mongoose');


//Allows our backend application to be available for use in our frontend application
//Allows us to control the app's Cross-Origin Resources Sharing settings
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');

const app = express();
const port = process.env.PORT || 4000;

//Middlewares
//Allows all resources to acess our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//URI
app.use("/users", userRoutes)
app.use("/courses", courseRoutes)

//Mongoose Connection
mongoose.connect(`mongodb+srv://gperey:admin123@zuitt-batch197.jvr3p9k.mongodb.net/s37-s41?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

const db = mongoose.connection

db.on('error', () => console.error('Connection Error'));
db.once('open', () => console.log('Connected to MongoDB'));


app.listen(port, () => console.log(`API is now online at port ${port}`));

