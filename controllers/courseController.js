const Course = require('../models/Course')
const auth = require('../auth');
const User = require('../models/User');

//Create a new course
module.exports.addCourse = (userData, reqBody) => {
	return User.findById(userData.id).then(result => {
		if(userData.isAdmin == false){
			return "You are not an admin"
		} else {
			let newCourse = new Course({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price,
			});

			return newCourse.save().then((course, error) => {
				if (error) {
					return false
				} else {
					return "Course Added successfully!!!"
				}
			})
		}
	})

}


module.exports.getAllCourses = (data) => {
	if (data.isAdmin) {
		return Course.find({}).then(result => {
			return result
		})
	} else {
		return false
	}
};


//Retrieves all ACTIVE courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result =>{
		return result
	})
}


//retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}



module.exports.updateCourse = (reqParams, reqBody, data) =>{

	if(data) {

		let updatedCourse = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		}
			// Syntax: findByIdAndUpdate(document ID, updatesToBeApplied)
				//
		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})

	} else {
		return "You are not an Admin!"
	}
};


//ACTIVITY

module.exports.courseArchive = (reqParams, reqBody, data) =>{

	if(data) {

		let archivedCourse = {
			isActive: reqBody.isActive
		}
			// Syntax: findByIdAndUpdate(document ID, updatesToBeApplied)
				//
		return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((archivedCourse, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})

	} else {
		return "You are not an Admin!"
	}
};