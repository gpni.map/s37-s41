const express = require('express')
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require('../auth')


//Route for creating a course
router.post('/', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
    console.log(userData)

	courseController.addCourse({userId: userData.id, isAdmin: userData.isAdmin}, req.body)
	.then(resultFromController => 
		res.send(resultFromController))
});



//route for retrieving all the courses
router.get("/all", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	courseController.getAllCourses(userData).then(resultFromController => res.send(resultFromController))
});



//Route for retrieving all ACTIVE courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
});


//Route for retrieving a specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
});


// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {

	const isAdminData = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdminData)

	courseController.updateCourse(req.params, req.body, isAdminData).then(resultFromController => res.send(resultFromController))
});


//ACTIVITY

router.put("/:courseId/archive", auth.verify, (req, res) => {

	const isAdminData = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdminData)

	courseController.courseArchive(req.params, req.body, isAdminData).then(resultFromController => res.send(resultFromController))
});


module.exports = router
